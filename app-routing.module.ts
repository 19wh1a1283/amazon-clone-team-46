import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import{ HomeComponent} from './PAGES/home/home.component';
import{ LoginComponent} from './COMPONENTS/login/login.component';
import { RegisterComponent } from './COMPONENTS/register/register.component';
import { WelcomeComponent } from './COMPONENTS/welcome/welcome.component';
// import{ CheckoutComponent} from './PAGES/checkout/checkout.component';



const routes: Routes = [  {path: '', component: HomeComponent},
  
  {path:'welcome',component:  WelcomeComponent},
  {path:'home',component: HomeComponent},
  {path:'login',component: LoginComponent},
  {path:'register',component: RegisterComponent},
  {path: 'search/:searchTerm', component: HomeComponent} 

  // {path:'checkout',component: CheckoutComponent},
   
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
