import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './COMPONENTS/header/header.component';
import { HomeComponent } from './PAGES/home/home.component';
import { FooterComponent } from './COMPONENTS/footer/footer.component';
//  import { CheckoutComponent } from './PAGES/checkout/checkout.component';
import { BannerComponent } from './COMPONENTS/banner/banner.component';
import { MatCardModule } from '@angular/material/card';

import { HttpClientModule } from '@angular/common/http';
import { ProductsComponent } from './COMPONENTS/products/products.component';
import { RegisterComponent } from './COMPONENTS/register/register.component';
import { LoginComponent } from './COMPONENTS/login/login.component';
import { WelcomeComponent } from './COMPONENTS/welcome/welcome.component';
// import { CartComponent } from './COMPONENTS/cart/cart.component';
// import { CheckoutSubtotalComponent } from './COMPONENTS/checkout-subtotal/checkout-subtotal.component';
// import { CheckoutProductsComponent } from './COMPONENTS/checkout-products/checkout-products.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    
    HomeComponent,
    FooterComponent,
    // CheckoutComponent,
    HeaderComponent,
    BannerComponent,
    ProductsComponent,
    RegisterComponent,
    LoginComponent,
    WelcomeComponent,
    // CartComponent,
    // CheckoutSubtotalComponent,
    // CheckoutProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
